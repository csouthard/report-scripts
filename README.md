# Report Scripts

This script will output the issues (and MRs) that have been closed in 
and Merge Requests that have been merged within a certain time frame.  
The tool is interactive and
will give you options as to what it will output.

To run this tool clone the repository. Add a
[`GITLAB_API_TOKEN`](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
in your environment by adding

```bash
export GITLAB_API_TOKEN='XXXXXXXX'
```
into your `.bashrc`

Then run

```bash
bundle
./report.rb
```

If `GITLAB_API_TOKEN` is not set, the tool will ask for this information.

Your output will look like this:
```bash
>> How many days ago? 7
>> Would you like to filter by group? yes
>> Which team(s)? Static analysis, Composition analysis
‣ ⬢ Composition analysis
  ⬢ Static analysis
  ⬡ Dynamic analysis
  ⬡ Vulnerability Research
>> Are you looking for certain labels? yes
>> Which labels are you looking for?
  feature
‣ P1,bug
>> Are you interested in MRs? Yes
Gathering data for Static analysis ...
Gathering data for Composition analysis ...
```

Note that by not providing any group in response to the group selection question `>> Which team(s)?`, 
the whole stage `devops::secure` will be considered.

A markdown file will be saved in the root with the path `report_YYYY-MM-DD.md`. The structure of that files follows 
the strcuture below:

``` md
##  Closed Issues
###  Group 1
###  Group 2

##  Merged MRs
###  Group 1
###  Group 2
```

---
Code based off https://gitlab.com/gitlab-org/async-retrospectives
